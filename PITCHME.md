# A brief history of Statistics

---

###  @css[headline](What are Statistics?)
#### @css[byline](And why do we care?)

@ul
- There are patterns in data
    - We can't always spot those patterns
- For us, statistics = "a collection of techniques that allow people to understand the patterns present in datasets"
- We'll consider two broad categories
    - Supervised
    - Unsupervised
@ulend

<!-- ![](assets/img/presentation.png) -->
---
### @css[headline](Variable (or Feature) types)

@ul[text-08]
- Categorical/Nominal: unordered categories
    - (e.g. female/male)
- Ordinal: ordered values, difference not easily quantified
    - (e.g. happiness scale, pain scale)
- Interval: differences are meaningful, but quotients aren't
    - (e.g. temperature)
- Ratio: the @color[gold](gold) standard; differences and quotients are meaningful
    - (e.g. distances, durations, masses)

- **NOTE: Independent vs. Dependent Variable is another important distinction, [see here](https://www.thoughtco.com/independent-and-dependent-variables-differences-606115) for more**
@ulend
---
### @css[headline](The Central Limit Theorem)

- The (Classical) [Central Limit Theorem](https://en.wikipedia.org/wiki/Central_limit_theorem) states: "the sum of a __random__ sample of *n* __independent and identically distributed__ random variables tends towards a Normal (or Gaussian) distribution as *n* becomes large"
- The definition of "large" is controversial; *n* = 30???
---
### @css[headline](Statistical Tests)
@ul[text-08]
- Null hypothesis: generally speaking, "there's no difference between populations"
- Alternative hypotheses: generally, "there's a measurable difference between populations"
- Common tests: [ANOVA](https://en.wikipedia.org/wiki/Analysis_of_variance), [Student's t-test](https://en.wikipedia.org/wiki/Student%27s_t-test)
- p-values are common, but dangerous
@ulend
---?image=https://imgs.xkcd.com/comics/significant.png&size=auto 95%
@snap[south]
Source: [XKCD](https://xkcd.com/882/)
@snapend
---
@snap[west span-30]
Snap span keywords are width-based, not height. Consider adding e.g. "span-h50" as a height-based alternative in custom CSS
@snapend
@snap[north span-30]
![IMAGE](https://imgs.xkcd.com/comics/significant.png)
@snapend
---
![](https://imgs.xkcd.com/comics/frequentists_vs_bayesians.png)
---
### @css[headline](Frequentists and Bayesians)

@ul
- Many common statistical tools - using mean and sd to characterize a distribution, calculating p-values using Z-scores, t-tests are based on an underlying assumption of Normality
@ulend
---
### @css[headline](Terminology)
@ul
- Sample
- Population
@ulend
---
### @css[headline](Additional Resources)

---
<!-- @title[Customize Slide Layout]

@snap[west span-50]
## Customize Slide Content Layout
@snapend

@snap[east span-50]
![](assets/img/presentation.png)
@snapend

---?color=#E58537
@title[Add A Little Imagination]

@snap[north-west]
#### Add a splash of @color[cyan](**color**) and you are ready to start presenting...
@snapend

@snap[west span-55]
@ul[spaced text-white]
- You will be amazed
- What you can achieve
- *With a little imagination...*
- And **GitPitch Markdown**
@ulend
@snapend

@snap[east span-45]
@img[shadow](assets/img/conference.png)
@snapend

---?image=assets/img/presenter.jpg

@snap[north span-100 headline]
## Now It's Your Turn
@snapend

@snap[south span-100 text-06]
[Click here to jump straight into the interactive feature guides in the GitPitch Docs @fa[external-link]](https://gitpitch.com/docs/getting-started/tutorial/)
@snapend
-->